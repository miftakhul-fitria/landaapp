app.controller("tpembelianCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_pembelian/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    $scope.cariSupplier = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_pembelian/supplier', {'nama': query}).then(function (response) {
                $scope.listSupplier = response.data.list;
            });
        }
    };
    $scope.cariBarang = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_pembelian/barang', {'nama': query}).then(function (response) {
                $scope.listBarang = response.data.list;
            });
        }
    };
    $scope.getDetail = function (id) {
        Data.get("t_pembelian/view?t_pembelian_id=" + id).then(function (response) {
            $scope.listDetail = response.data;
        });
    };

    $scope.listDetail = [{}];

    $scope.getDetail = function (form) {
        console.log(form);
        Data.get("t_pembelian/view/" + form).then(function (data) {
            $scope.listDetail = data.data;
            console.log(data);
            $scope.getSubTotal(); //untuk menampilkan subtotal ketika di klik button update
        });
    };

    $scope.getSubTotal = function () {
        var total = 0;
        angular.forEach($scope.listDetail, function (value, key) {
            value.subTotal = value.jumlah * value.harga;
            total = total + value.subTotal;
        });
        $scope.form.total = total;
    }

    $scope.addDetail = function (listDetail, barang=undefined) {
        // var comArr = eval(listBarang);
        var newDet = {};
        if (barang != undefined){
            newDet = barang;
        }
        $scope.listDetail.push(newDet);
    };
    $scope.removeDetail = function (val, paramindex) {
        var comArr = eval(val);
        if (comArr.length > 1) {
            val.splice(paramindex, 1);
        } else {
            alert("Something gone wrong");
        }
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.listDetail = [{}];
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.tanggal;
        $scope.form = form;
        $scope.form.tanggal = new Date(form.tanggal);
        $scope.getDetail(form.id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.tanggal;
        $scope.form = form;
        $scope.form.tanggal = new Date(form.tanggal);
        $scope.getDetail(form.id);
    };
    $scope.save = function (form, status) {
        $scope.loading = true;
        form.status = status;
        var params = {
            data: form,
            detail: $scope.listDetail
        }
        Data.post("t_pembelian/save", params).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.delete = function (row) {
        console.log(row);
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            row.is_deleted = 0;
            Data.post("t_pembelian/hapus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                if (result.status_code == 200) {
                    $rootScope.alert("Berhasil", "Data berhasil dihapus", "success");
                    $scope.cancel();
                } else {
                    $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                }
            });
        }
    };

    //Untuk menambahkan modal barang
    $scope.modalBarang = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_pembelian/modalBarang.html",
            controller: "barangCtrl",
            size: "md",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: {},
            }
        });
        modalInstance.result.then(function (response) {
            console.log(response)
            if (response.data == undefined) {
            } else {
                console.log("data yang diterima dari modal",response)
                var newDet = {
                    m_barang_id : response.data
                }
                $scope.addDetail($scope.listBarang, newDet)
            }
        });
    };
    //Untuk menambahkan modal barang - END
    
    //Untuk menambahkan modal supplier
    $scope.modalSupplier = function (m_supplier_id) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_pembelian/modalSupplier.html",
            controller: "supplierCtrl",
            size: "md",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: {
                    m_supplier_id : m_supplier_id
                },
            }
        });
        modalInstance.result.then(function (response) {
            console.log(response)
            if (response.data == undefined) {
            } else {
                console.log("data yang diterima dari modal",response)
                $scope.form.m_supplier_id = response.data;
            }
        });
    }
    //Untuk menambahkan modal supplier - END
});

app.controller("barangCtrl", function ($state, $scope, Data, $uibModalInstance, $rootScope, form) {
    $scope.form = {};

    Data.get('m_kategori/index', {filter: {"m_kategori.is_deleted": 0}}).then(function (response) {
        $scope.listKategori = response.data.list;
    });

    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("m_barang/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                console.log("data yang dikirim ketika modal on close",result);

                $uibModalInstance.close({
                    'data': result.data
                });
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});

app.controller("supplierCtrl", function ($state, $scope, Data, $uibModalInstance, $rootScope, form) {
    console.log(form);
    $scope.form = {};
    $scope.form = form.m_supplier_id != undefined ? form.m_supplier_id : form; //untuk menampilkan data berdasarkan id

    Data.get('m_supplier/index', {filter: {"m_supplier.is_deleted": 0}}).then(function (response) {
        $scope.listSupplier = response.data.list;
        console.log($scope.listSupplier);
    });

    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("m_supplier/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                console.log("data yang dikirim ketika modal on close",result);

                $uibModalInstance.close({
                    'data': result.data
                });
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});