app.controller("mahasiswaCtrl", function($scope, Data, $rootScope) {
    $scope.filter = {};
    $scope.tampilkan = false;

    $scope.getLaporan = function (filter) {
        console.log(filter);
        Data.get("l_mahasiswa/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.listMahasiswa = data.data.list;
                // console.log($scope.listMahasiswa);
                // console.log(data);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }

        });
    };

    // Untuk select option jurusan
    Data.get("l_mahasiswa/daftar_jurusan").then(function (data) {
        if (data.status_code == 200) {
            $scope.listJurusan = data.data.list;
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }

    });
    // Untuk select option jurusan - END

    $scope.getListKelas = function (m_jurusan_id) {
        console.log("parameter yg dikirim", m_jurusan_id);
        Data.get("l_mahasiswa/kelas/" + m_jurusan_id).then(function (data) {
            if (data.status_code == 200) {
                $scope.listKelas = data.data.list;
                // console.log($scope.listKelas);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }

        });
    }
});