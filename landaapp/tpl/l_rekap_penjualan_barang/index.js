app.controller("laporanBarangCtrl", function ($scope, Data, $rootScope) {

    $scope.filter = {};
    $scope.filter.bulan = new Date();
    $scope.tampilkan = false;

    $scope.getRekap = function (filter) {
        Data.get("l_rekap_penjualan_barang/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.listPenjualan = data.data.list;
                $scope.result = data.data.result;
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };

    // Untuk select option nama barang
    Data.get("l_rekap_penjualan_barang/barang_nama").then(function (data) {
        if (data.status_code == 200) {
            $scope.listBarang = data.data.list;
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }

    });
    // Untuk select option nama barang - END

    // Untuk select option nama customer
    Data.get("l_rekap_penjualan_barang/customer_nama").then(function (data) {
        if (data.status_code == 200) {
            $scope.listCustomer = data.data.list;
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }

    });
    // Untuk select option nama customer - END

    /**
     * Ambil laporan dari server
     */
    $scope.view = function (is_export, is_print) {

        var param = {
            is_export: is_export,
            is_print: is_print,
            bulan: moment($scope.filter.bulan).format('YYYY-MM'),
//            is_ppn: $scope.form.is_ppn
        };


        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/laporan', param).then(function (response) {
                console.log(response)
                if (response.status_code == 200) {
                    $scope.data = response.data.data;
                    $scope.detail = response.data.detail;
                    $scope.tampilkan = true;
                } else {
                    $scope.tampilkan = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "api/l_rekap_penjualan_barang/view?" + $.param(param), "_blank");
            });
        }
    };
});