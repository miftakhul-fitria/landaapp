app.controller("tkelompokkelasCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_kelompok_kelas/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    $scope.cariJurusan = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_kelompok_kelas/jurusan', {'nama': query}).then(function (response) {
                $scope.listJurusan = response.data.list;
            });
        }
    };
    $scope.cariKelas = function (query) {
        console.log(query)
        var params = {
            nama: query,
            jurusan_id: $scope.form.m_jurusan_id["id"]
        }
        if (query.length >= 3) {
            Data.get('t_kelompok_kelas/kelas', params).then(function (response) {
                $scope.listKelas = response.data.list;

            });
        }
    };
    $scope.cariMahasiswa = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_kelompok_kelas/mahasiswa', {'nama': query}).then(function (response) {
                $scope.listMahasiswa = response.data.list;
            });
        }
    };
    $scope.getDetail = function (id) {
        Data.get("t_kelompok_kelas/view?t_kelompok_kelas_id=" + id).then(function (response) {
            $scope.listDetail = response.data;
        });
    };
    $scope.listDetail = [{}];

    $scope.getDetail = function (form) {
        console.log('test');
        console.log(form);
        Data.get("t_kelompok_kelas/view/" + form).then(function (data) {
            $scope.listDetail = data.data;
            console.log(data);
        });
    };

    $scope.getListKelas = function (m_jurusan_id) {
        // console.log("jurusan_id",m_jurusan_id);
        Data.get("t_kelompok_kelas/getListKelas/" + m_jurusan_id["id"]).then(function (result) {
            if (result.status_code == 200) {
                $scope.listKelas = result.data.list;
                console.log($scope.listKelas);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            // console.log(data);
        });
    };

    $scope.addDetail = function (val) {
        var comArr = eval(val);
        var newDet = {};
        val.push(newDet);
    };
    $scope.removeDetail = function (val, paramindex) {
        var comArr = eval(val);
        if (comArr.length > 1) {
            val.splice(paramindex, 1);
        } else {
            alert("Something gone wrong");
        }
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.listDetail = [{}]; //untuk mengkosongkan mahasiswa ketika buat baru
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.m_jurusan_id.nama;
        $scope.form = form;
        $scope.getDetail(form.id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.m_jurusan_id;
        $scope.form = form;
        $scope.getDetail(form.id);
    };
    $scope.save = function (form) {
        $scope.loading = true;
        var params = {
            data: form,
            detail: $scope.listDetail
        }
        console.log(params)
        Data.post("t_kelompok_kelas/save", params).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            row.is_deleted = 0;
            Data.post("t_kelompok_kelas/hapus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                if (result.status_code == 200) {
                    $rootScope.alert("Berhasil", "Data berhasil dihapus", "success");
                    $scope.cancel();
                } else {
                    $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
                }
            });
        }
    };
});
