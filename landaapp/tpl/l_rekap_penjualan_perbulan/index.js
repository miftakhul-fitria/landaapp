app.controller("penjualanCtrl", function($scope, Data, $rootScope) {

    $scope.filter = {};
    $scope.filter.bulan = new Date();
    $scope.tampilkan = false;

    $scope.getRekap = function (filter) {
        Data.get("l_rekap_penjualan_perbulan/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.listPenjualan= data.data.list;
                $scope.listDate = data.data.date;
                $scope.listTotal = data.data.totalHari;
                $scope.listTotalBarang = data.data.totalBarang;
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };

    // Untuk select option nama barang
    Data.get("l_rekap_penjualan_perbulan/barang_nama").then(function (data) {
        if (data.status_code == 200) {
            $scope.listBarang = data.data.list;
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }

    });
    // Untuk select option jurusan - END
});