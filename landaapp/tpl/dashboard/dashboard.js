angular.module('app').controller('dashboardCtrl', function ($scope, Data, $state, UserService, $location) {
    var user = UserService.getUser();
    if (user === null) {
        $location.path('/login');
    }

    Data.get("dashboard/barChart").then(function (response) {
        $scope.myBarChart = {
            type: "hbar",
            title: {
                text: "Daftar Barang Yang Paling Laku"
            },
            plotarea: {
                'adjust-layout': true,
                'offset-y': 50,
                'offset-x': 50
            },
            'scale-x': {
                label: { /* Scale Title */
                    text: "Nama Barang",
                },
                labels: response.data.namaBarang /* Scale Labels */
            },
            'scale-y': {
                label: { /* Scale Title */
                    text: "Jumlah"
                }
            },
            series: [
                {
                    values: response.data.jumlahBarang,
                    backgroundColor : "#2e7869 #2da28a"
                }
            ]
        };
    });

    Data.get("dashboard/lineChart").then(function (response) {
        // console.log(response.data.bulan);
        $scope.myLineChart = {
            type: "line",
            utc: true,
            title: {
                text: "Daftar Barang Per Kategori"
            },
            "legend" : {

            },
            plot : {
                'line-style': "dashdot",
                marker : {
                    "type": "circle",
                    "size": 5
                }
            },
            plotarea: {
                'adjust-layout': true
            },
            'scale-x': {
                label: { /* Scale Title */
                    text: "Bulan",
                },
                step : "month",
                transform : {
                    type: "date",
                    all: "%M"
                }
            },
            series: response.data.data
        };
    });
});