<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama"       => "required",
        "alamat"       => "required",
        "no_telp"       => "required",
    );
    GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_sekolah/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_sekolah")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil data user untuk update profil
 */
$app->get("/m_sekolah/view/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db   = $this->db;
    $id = $request->getAttribute('id');
    $db->select("*")
        ->from("m_siswa")
        ->where("m_sekolah_id", "=", $id);
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_sekolah/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("m_sekolah.*")
        ->from("m_sekolah");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_sekolah.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_sekolah.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_sekolah/save", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;

    $validasi = validasi($data['form']);
    if ($validasi === true) {
        try {
            if (isset($data['form']["id"])) {
                $db->delete("m_siswa", ['m_sekolah_id' => $data['form']['id']]);
                $model = $db->update("m_sekolah", $data['form'], ["id" => $data['form']["id"]]);
            } else {
                $model = $db->insert("m_sekolah", $data['form']);
            }

            if (isset($data['detail']) && !empty($data['detail'])) {
                foreach ($data['detail'] as $key => $value) {
                    $value['m_sekolah_id'] = $model->id;
                    $db->insert("m_siswa", $value);

                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/m_sekolah/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    unset($data["password"]);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_sekolah", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});