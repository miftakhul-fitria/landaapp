<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
        "m_jurusan_id" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_kelas/jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_jurusan")
        ->where("nama", "like", $params["nama"])
        ->andWhere("m_jurusan.is_deleted", "=", 0);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});
/**
 * Ambil semua m kelas
 */
$app->get("/m_kelas/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_kelas.*, m_jurusan.nama AS nama_jurusan")
        ->from("m_kelas")
        ->join("left join", "m_jurusan", "m_kelas.m_jurusan_id=m_jurusan.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
//    print_r($models);
//    die;
    foreach ($models as $key=>$value) {
//        print_r($value);
//        die;
        $models[$key]->m_jurusan_id = [
            "id"=>$value->m_jurusan_id,
            "nama"=>$value->nama_jurusan
        ];
    }

    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * Save m kelas
 */
$app->post("/m_kelas/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['m_jurusan_id'] = $data['m_jurusan_id']['id']; //Untuk save ui-select
            if (isset($data["id"])) {
                $model = $db->update("m_kelas", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_kelas", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m customer
 */
$app->post("/m_kelas/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['m_jurusan_id'] = $data['m_jurusan_id']['id'];
            $model = $db->update("m_kelas", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
