<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "m_kategori_id" => "required",
        "nama" => "required",
        "stok" => "required",
        "satuan" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_barang/kategori", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_kategori")->where("nama", "like", $params["nama"])
        ->andWhere("m_kategori.is_deleted", "=", 0);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

/**
 * Ambil semua m barang
 */
$app->get("/m_barang/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
        m_barang.*,
        m_kategori.nama AS kategori_nama")
        ->from("m_barang")
        ->join("left join", "m_kategori", "m_kategori.id=m_barang.m_kategori_id");
//        ->where("m_barang.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    foreach ($models as $key=>$value){
//        print_r($value);
//        die;
        $models[$key]->m_kategori_id = [
          "id"=>$value->m_kategori_id,
          "nama"=>$value->kategori_nama
        ];
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m barang
 */
$app->post("/m_barang/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['m_kategori_id'] = $data['m_kategori_id']['id']; //Untuk save ui-select
            if (isset($data["id"])) {
                $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_barang", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m barang
 */
$app->post("/m_barang/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['m_kategori_id'] = $data['m_kategori_id']['id']; //untuk menghapus ui-select
            $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});