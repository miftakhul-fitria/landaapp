<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "tanggal" => "required",
        "m_supplier_id" => "required",
        "tanggal" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_pembelian/supplier", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_supplier")
        ->where("nama", "like", $params["nama"])
        ->andWhere("m_supplier.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

$app->get("/t_pembelian/barang", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_barang")
        ->where("nama", "like", $params["nama"])
        ->andWhere("m_barang.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

/**
 * Ambil detail t pembelian
 */
$app->get("/t_pembelian/view/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute('id');
    $db->select("
        t_pembelian_det.*,
        m_barang.nama AS barang_nama")
        ->from("t_pembelian_det")
        ->join("left join", "m_barang", "m_barang.id=t_pembelian_det.m_barang_id")
        ->where("t_pembelian_id", "=", $id);
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key]->m_barang_id = [
            "id" => $value->m_barang_id,
            "nama" => $value->barang_nama
        ];
    }
    return successResponse($response, $models);
});
/**
 * Ambil semua t pembelian
 */
$app->get("/t_pembelian/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
        t_pembelian.*,
        m_supplier.nama AS supplier_nama,
        m_supplier.no_telp AS supplier_telepon,
        m_supplier.alamat AS supplier_alamat,
        m_supplier.is_deleted AS supplier_is_deleted")
        ->from("t_pembelian")
        ->join("left join", "m_supplier", "m_supplier.id=t_pembelian.m_supplier_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key]->m_supplier_id = [
            "id" => $value->m_supplier_id,
            "nama" => $value->supplier_nama,
            "no_telp" =>$value->supplier_telepon,
            "alamat" => $value->supplier_alamat,
            "is_deleted" => $value->supplier_is_deleted
        ];
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t pembelian
 */
$app->post("/t_pembelian/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            // strotime -> merubah string (tanggal) menjadi tanggal dalam satuan Integer / Unix time / timestamp --> 150912378
            // February 09 01 thursday -> 150912378 -> Y-m-d -> 0000-00-00
            $data['data']['tanggal'] = date("Y-m-d", strtotime($data['data']['tanggal']));
            $data['data']['m_supplier_id'] = $data['data']['m_supplier_id']['id']; //Untuk save ui-select
//            print_r($data);
//            die;

            if (isset($data["data"]["id"])) {
                $model = $db->update("t_pembelian", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("t_pembelian_det", ["t_pembelian_id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("t_pembelian", $data["data"]);
            }
            /**
             * Simpan detail
             */
            if (isset($data["detail"]) && !empty($data["detail"])) {
                foreach ($data["detail"] as $key => $val) {
                    $detail["m_barang_id"] = $val["m_barang_id"]["id"];
                    $detail["jumlah"] = $val["jumlah"];
                    $detail["harga"] = $val["harga"];
//                    $detail["subTotal"] = $val["subTotal"];
                    $detail["t_pembelian_id"] = $model->id;
//                    print_r($detail);
//                    die;
                    $db->insert("t_pembelian_det", $detail);
                    if ($model->status == "tersimpan") {
                        $db->run("UPDATE m_barang SET stok = stok+". $val['jumlah'] ." WHERE id = " . $val["m_barang_id"]["id"]);
                    }
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus t pembelian
 */
$app->post("/t_pembelian/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $data['m_supplier_id'] = $data['m_supplier_id']['id']['nama'];
        $modelDetail = $db->delete("t_pembelian_det", ["t_pembelian_id" => $data["id"]]);
        $model = $db->delete("t_pembelian", ["id" => $data["id"]]);

        return successResponse($response, $modelDetail);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
