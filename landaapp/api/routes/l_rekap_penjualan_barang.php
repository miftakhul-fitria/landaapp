<?php
/**
 * Ambil semua list user
 */
$app->get("/l_rekap_penjualan_barang/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $tanggal_awal = date("Y-m-01", strtotime($params['bulan']));
    $tanggal_akhir = date("Y-m-t", strtotime($params['bulan'])); //t : ambil tanggal paling akhir di bulan tsb
//    echo $tanggal_awal;
//    echo $tanggal_akhir;
//    die;

    $db->select("
        t_penjualan.*,
        t_penjualan.tanggal AS penjualan_tanggal,
        m_barang.nama AS barang_nama,
        m_barang.id AS m_barang_id,
        m_customer.nama AS customer_nama,
        t_penjualan_det.jumlah AS detail_jumlah,
        t_penjualan_det.harga AS detail_harga,
        m_barang.satuan AS barang_satuan")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "m_barang.id=t_penjualan_det.m_barang_id")
        ->join("left join", "m_customer", "m_customer.id=t_penjualan.m_customer_id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->andWhere("tanggal", ">=", $tanggal_awal)
        ->andWhere("tanggal", "<=", $tanggal_akhir)
        ->findAll();

    if (isset($params["customer"]) && !empty($params["customer"])) {
        $db->where("m_customer.id", "=", $params["customer"]);
    }
    if (isset($params["barang"]) && !empty($params["barang"])) {
        $db->where("m_barang.id", "=", $params["barang"]);
    }

    $models = $db->findAll();

    $jumlahBarang = 0;
    $subTotal = 0;
    $total = 0;
    $result = [];
    foreach ($models as $key => $value) {
        $jumlahBarang = $jumlahBarang + $value->detail_jumlah;
        $subTotal = $value->detail_jumlah * $value->detail_harga;
        $models[$key]->subTotal = $subTotal; //di masukkan ke $models
        $total = $total + $subTotal;

        $result["jumlahBarang"] = $jumlahBarang;
        $result["total"] = $total;
    };
    $totalItem = $db->count();

    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/penjualan_barang.xlsx");
//        echo "berhasil";
//        die;

        $sheet = $xls->getSheet(0);
        $sheet->getCell('A2')->setValue(date('F Y', strtotime($params['bulan'])));
        $index = 5;
        $no = 1;

        foreach ($models as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['penjualan_tanggal']);
            $sheet->getCell('C' . $index)->setValue($value['customer_nama']);
            $sheet->getCell('D' . $index)->setValue($value['barang_nama']);
            $sheet->getCell('E' . $index)->setValue($value['detail_jumlah']);
            $sheet->getCell('F' . $index)->setValue($value['barang_satuan']);
            $sheet->getCell('G' . $index)->setValue($value['detail_harga']);
            $sheet->getCell('H' . $index)->setValue($value['subTotal']);

            $index++;
        }

        $sheet->mergeCells("A" . $index . ":D" . $index);
        $sheet->getCell('A' . $index)->setValue('TOTAL ');
        $sheet->getCell('E' . $index)->setValue($result['jumlahBarang']);
        $sheet->getCell('H' . $index)->setValue($result['total']);

        $sheet->getStyle('A' . $index . ":H" . $index)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '00ff19')
                )
            )
        );

        $sheet->getStyle("A" . 4 . ":H" . $index)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"LAPORAN PENJUALAN " . date("F Y", strtotime($params['bulan'])) . ".xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;
    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("laporan/penjualan.html", [
            'data' => $models,
            'result' => $result,
            'month' => $params
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    }

    return successResponse($response, ["list" => $models, "result" => $result, "totalItems" => $totalItem]);
});

$app->get("/l_rekap_penjualan_barang/barang_nama", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_barang")
        ->where("m_barang.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});

$app->get("/l_rekap_penjualan_barang/customer_nama", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_customer")
        ->where("m_customer.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});