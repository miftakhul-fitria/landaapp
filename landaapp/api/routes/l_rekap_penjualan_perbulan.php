<?php
/**
 * Ambil semua list user
 */
$app->get("/l_rekap_penjualan_perbulan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $tanggal_awal = date("Y-m-01", strtotime($params['bulan']));
    $tanggal_akhir = date("Y-m-t", strtotime($params['bulan'])); //t : ambil tanggal paling akhir di bulan tsb
//    echo $tanggal_awal;
//    echo $tanggal_akhir;
//    die;

    $db->select("
        m_barang.*,
        m_barang.nama AS barang_nama,
        m_barang.id AS m_barang_id,
        t_penjualan.tanggal,
        SUM(t_penjualan_det.jumlah) AS jumlah")
        ->from("m_barang")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.m_barang_id=m_barang.id")
        ->join("left join", "t_penjualan", "t_penjualan.id=t_penjualan_det.t_penjualan_id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->andWhere("tanggal", ">=", $tanggal_awal)
        ->andWhere("tanggal", "<=", $tanggal_akhir)
        ->groupBy("m_barang.id, tanggal");


    if (isset($params["barang"]) && !empty($params["barang"])) {
        $db->where("m_barang.id", "=", $params["barang"]);
    }

    $models = $db->findAll();

    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_barang_id] ["m_barang_id"] = $value->m_barang_id;
        $result[$value->m_barang_id] ["barang_nama"] = $value->barang_nama;
        $result[$value->m_barang_id] ["jumlah"] = $value->jumlah;
        $result[$value->m_barang_id] ["listPenjualan"] [$value->tanggal] = (array)$value;
    }

    $date = $tanggal_awal;
    $end = $tanggal_akhir; //get end date of month

    $listDate = [];
    while(strtotime($date) <= strtotime($end)) {
        $listDate[] = $date;
        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
    }

    $totalHari = []; //untuk total barang/hari
    $totalBarang = []; //untuk total/barang
    foreach ($result as $key => $value) {
        foreach ($listDate as $valDate) {
            if (empty($value["listPenjualan"][$valDate]["jumlah"])){
                $result[$key]["listPenjualan"][$valDate]["jumlah"] = 0;
            }
            @$totalHari[$valDate] += $result[$key]["listPenjualan"][$valDate]["jumlah"];
            @$totalBarang[$value["m_barang_id"]] += $result[$key]["listPenjualan"][$valDate]["jumlah"];
        }
        ksort($result[$key]["listPenjualan"]); //untuk mengurutkan data sesuai tanggal-nya
    }
    
//    print_r($listDate);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "date" => $listDate, "totalHari" =>$totalHari, "totalBarang" => $totalBarang, "totalItems" => $totalItem]);
});

$app->get("/l_rekap_penjualan_perbulan/barang_nama", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_barang")
        ->where("m_barang.is_deleted", "=", 0);

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});