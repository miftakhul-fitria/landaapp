<?php
/**
 * Ambil semua list user
 */
$app->get("/l_mahasiswa/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
        m_mahasiswa.*,
        m_kelas.nama AS kelas_nama,
        m_kelas.id AS m_kelas_id,
        m_jurusan.nama AS jurusan_nama,
        m_jurusan.alamat AS jurusan_alamat,
        m_jurusan.no_telepon AS jurusan_telepon")
        ->from("m_jurusan")
        ->join("left join", "t_kelompok_kelas", "t_kelompok_kelas.m_jurusan_id=m_jurusan.id")
        ->join("left join", "m_kelas", "m_kelas.id=t_kelompok_kelas.m_kelas_id")
        ->join("left join", "t_kelompok_kelas_det", "t_kelompok_kelas_id=t_kelompok_kelas.id")
        ->join("left join", "m_mahasiswa", "m_mahasiswa.id=t_kelompok_kelas_det.m_mahasiswa_id");

    if (isset($params["kelas"]) && !empty($params["kelas"])) {
        $db->where("m_kelas.id", "=", $params["kelas"]);
    }

    if (isset($params["jurusan"]) && !empty($params["jurusan"])) {
        $db->where("m_jurusan.id", "=", $params["jurusan"]);
    }

    $models = $db->findAll();
//    print_r($models);
//    die;

    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_kelas_id] ["m_kelas_id"] = $value->m_kelas_id;
        $result[$value->m_kelas_id] ["jurusan_nama"] = $value->jurusan_nama;
        $result[$value->m_kelas_id] ["jurusan_alamat"] = $value->jurusan_alamat;
        $result[$value->m_kelas_id] ["jurusan_telepon"] = $value->jurusan_telepon;
        $result[$value->m_kelas_id] ["kelas_nama"] = $value->kelas_nama;
        if (!empty($value->nim)) {
            $result[$value->m_kelas_id] ["listMahasiswa"] [] = $value;
        }
    }
//    print_r($result);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "totalItems" => $totalItem]);
});

$app->get("/l_mahasiswa/daftar_jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_jurusan");

    $models = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models]);
});

$app->get("/l_mahasiswa/kelas/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute('id');
    $db->select("m_kelas.*")
        ->from("m_kelas")
        ->join("left join","m_jurusan","m_kelas.m_jurusan_id=m_jurusan.id")
        ->where("m_jurusan_id", "=", $id);

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});