<?php
/**
 * Ambil semua list user
 */
$app->get("/l_siswa/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
        m_siswa.*,
        m_sekolah.id AS m_sekolah_id,
        m_sekolah.nama AS sekolah_nama, 
        m_sekolah.alamat AS sekolah_alamat, 
        m_sekolah.no_telp AS sekolah_telepon")
        ->from("m_sekolah")
        ->join("left join", "m_siswa", "m_siswa.m_sekolah_id=m_sekolah.id");


    if(isset($params["jenisSekolah"]) && !empty($params["jenisSekolah"])) {
        $db->where("m_sekolah.jenis", "=", $params["jenisSekolah"]);
    }

    $models = $db->findAll();
//    print_r($models);
//    die;

    $result = [];
    foreach ($models as $key => $value) {
     $result[$value->m_sekolah_id] ["m_sekolah_id"] = $value->m_sekolah_id;
     $result[$value->m_sekolah_id] ["sekolah_nama"] = $value->sekolah_nama;
     $result[$value->m_sekolah_id] ["sekolah_alamat"] = $value->sekolah_alamat;
     $result[$value->m_sekolah_id] ["sekolah_telepon"] = $value->sekolah_telepon;
     if (!empty($value->nis)) {
         $result[$value->m_sekolah_id] ['listSiswa'] [] = $value;
     }
    }
//    print_r($result);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "totalItems" => $totalItem]);
});