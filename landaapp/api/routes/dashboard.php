<?php
$app->get("/dashboard/barChart", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
        m_barang.*,
        m_barang.nama AS barang_nama,
        m_kategori.nama AS kategori_nama,
        SUM(t_penjualan_det.jumlah) AS jumlah")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "m_barang.id=t_penjualan_det.m_barang_id")
        ->join("left join", "m_kategori", "m_kategori.id=m_barang.m_kategori_id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->groupBy("m_barang.nama")
        ->orderBy("jumlah DESC")
        ->limit('5');

    $models = $db->findAll();

    $nama_barang = [];
    $jumlah_barang = [];
    foreach ($models as $key => $value) {
        array_push($nama_barang, $value->barang_nama);
        array_push($jumlah_barang, $value->jumlah);
    }
    $jumlah_barang = array_map('intval', $jumlah_barang);
    array_multisort($jumlah_barang, SORT_ASC, $nama_barang, SORT_DESC, $models);

//    print_r($models);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["namaBarang" => $nama_barang, "jumlahBarang" => $jumlah_barang , "totalItems" => $totalItem]);
});

$app->get("/dashboard/lineChart", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    // $tanggal_awal = date("Y-01");
    // $tanggal_akhir = date("Y-12");

    $db->select("
        m_kategori.nama AS kategori_nama,
        m_kategori.id AS kategori_id,
        SUM(t_penjualan_det.jumlah) AS detail_jumlah,
        MONTH(t_penjualan.tanggal) AS penjualan_bulan")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "m_barang.id=t_penjualan_det.m_barang_id")
        ->join("left join", "m_kategori", "m_kategori.id=m_barang.m_kategori_id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->groupBy("m_kategori.id, MONTH(t_penjualan.tanggal)")
        ->orderBy("penjualan_bulan DESC");

    $models = $db->findAll();
    // print_r($models);
    // die;

    $kategori = [];
    $result = [];
    $bulan = range(1, 12);
    $data = [];
    $dataBaru = [];

    foreach($models as $key => $value) {
        // print_r($result[$value->kategori_nama]);
        // die;
        // check redundancy data of category
        if(!in_array($value->kategori_nama, $kategori)){
            array_push($kategori, $value->kategori_nama);
        }
        $result[$value->kategori_nama][$value->penjualan_bulan] = intval($value->detail_jumlah);
    }
//     print_r($models);
//     die;


    $kategoriBarang = [];
    foreach($models as $key => $value) {
        $kategoriBarang[$value->kategori_id]["penjualan_bulan"] = $value->detail_jumlah;
        $kategoriBarang[$value->kategori_id]["kategori_nama"] = $value->kategori_nama;
//        $kategoriBarang[$value->penjualan_bulan] = $value->detail_jumlah;
//         $kategoriBarang[$value->penjualan_bulan] = $value->penjualan_bulan;
    }
//     print_r($kategoriBarang);
//     die;

    foreach ($result as $key => $value) {
        foreach ($bulan as $k => $v) {
            if(!isset($result[$key][$v])) {
                $result[$key][$v] = 0;
            }
        }
        ksort($result[$key]);
        $data['values'] = array_values($result[$key]);
        $dataBaru[] = $data;
    }
    // print_r($dataBaru);
    // die;

    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "data" => $dataBaru, "kategoriBarang" => $kategoriBarang, "totalItems" => $totalItem]);
});
?>